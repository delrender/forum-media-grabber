# forum-media-grabber
Wakaba-style imageboards files multithreaded parser

Now with GUI!

### Has three modes:
1. download files
2. creating XSPF/M3U playlist with .webm and YouTube links which then can be played in media players like VLC or SMPlayer
3. collecting external links in .html file with external sites screenshots

### Hotkeys in Watch Mode
__Alt+2__ `COPY LINK`

__Alt+3__ `BAN LINK`

__Alt+5__ `SAVE TO DISK`

##Installation
```sh
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt 
```


