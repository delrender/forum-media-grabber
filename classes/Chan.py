from urllib.parse import urlparse
import re

class Chan:
    def __init__(self, id, url, scope=''):
        self.id = id
        self.url = url
        self.scope = scope
        self.topic = False
        self.get_chan_scope()

    def get_chan_scope(self):
        chan_url_components = urlparse(self.url)
        if chan_url_components.path != '/':
            self.scope = 'topic'
            regex_topic = r"\/.*\/"
            match_topic = re.findall(regex_topic, chan_url_components.path)
            if len(match_topic) > 0:
                self.topic = match_topic[0]
