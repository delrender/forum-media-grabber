from PyQt5.QtWidgets import QApplication
import time


class GuiUtils:

    @staticmethod
    def sleep_with_update_gui(seconds):
        for n in range(seconds):
            QApplication.processEvents()
            time.sleep(1)

    @staticmethod
    def update_gui():
        QApplication.processEvents()
