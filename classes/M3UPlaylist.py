from lxml import etree as ET
import threading
import os

lock = threading.Lock()


class M3UPlaylist:

    template = ''
    filepath = os.path.dirname(os.path.abspath(__file__))
    playlist_path = os.path.join(filepath, '../lain.m3u')

    def __init__(self, playlist_path, continue_parsing=None):
        self.playlist_path = playlist_path
        self.continue_parsing = continue_parsing
        self.xml_doc = None
        self.root_xml_doc = None
        self.tracklist = None
        self.init_xml_playlist(self.playlist_path)

    def get_template(self):
        self.template = ''

    def init_xml_playlist(self, filepath):
        if not self.continue_parsing:
            self.get_template()
            with open(filepath, "w") as text_file:
                text_file.write(self.template)
        print('Playlist initiated')

    def add_track(self, link=None, page_link=None):
        f = open(self.playlist_path, "a")
        # @todo track info
        # f.write("#EXTINF:-1,"+page_link+"\r\n"+link+"\r\n")
        f.write(link+"\r\n")
        f.close()
        print('track added!!!\n')

    @staticmethod
    def remove_track(link):
        with open(M3UPlaylist.playlist_path, "r+") as f:
            lines = f.readlines()
            f.seek(0)
            for line in lines:
                if line.strip("\n") != link:
                    f.write(line)
            f.truncate()