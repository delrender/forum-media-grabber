import time
import threading


class PageThread(threading.Thread):
    def __init__(self, id, name, i):
        threading.Thread.__init__(self)
        self.id = id
        self.name = name
        self.i = i

    def run(self):
        thread_test(self.name, self.i, 5)
        print("%s has finished execution " % self.name)