import os
import re
import sqlite3
import time
from threading import *
from time import sleep
from urllib.parse import urljoin, urlparse, quote
from urllib.request import urlopen

import requests
from bs4 import BeautifulSoup
from requests.exceptions import HTTPError, ConnectionError
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from classes.Chan import Chan
from classes.XspfPlaylist import XspfPlaylist
from classes.M3UPlaylist import M3UPlaylist
from classes.GuiUtils import GuiUtils


# @todo playlist select/edit tool, removing links from selected boards
class Parser:
    chans = []
    ban_list = []
    inner_pages = []

    filepath = os.path.dirname(os.path.abspath(__file__))
    links_file_path = os.path.join(filepath, '../links.html')
    links_screenshot_folder_path = './links_screenshots/'
    db = os.path.join(filepath, '../chan')

    def __init__(self, mode, parse_filetype=None, continue_parsing=None, threads=None):
        self.mode = mode
        self.parse_filetype = parse_filetype
        self.continue_parsing = continue_parsing
        self.threads = threads
        self.db_conn = sqlite3.connect(self.db, isolation_level=None, check_same_thread=False)
        print("Opened database successfully")

        self.clear_run()
        self.get_chans()
        self.get_ban_list()
        self.useragent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) ' \
                         'Chrome/80.0.3987.132 Safari/537.36 '
        if self.mode == 'xspf':
            self.XspfPlaylist = XspfPlaylist(os.path.join(self.filepath, '../lain.xspf'), self.continue_parsing)
        if self.mode in ('m3u', 'm3u_SFW'):
            self.M3UPlaylist = M3UPlaylist(os.path.join(self.filepath, '../lain.m3u'), self.continue_parsing)
        if self.mode == 'links':
            self.selenium_options = Options()
            self.selenium_options.headless = True
            self.selenium_options.add_argument("window-size=1900,1080")
            self.driver = webdriver.Firefox(firefox_options=self.selenium_options)
            self.driver.set_page_load_timeout(7)

    def get_chans(self):
        cursor = self.db_conn.execute("SELECT * from chans WHERE enabled=1")
        self.chans = []
        for row in cursor:
            self.chans.append(row)

    def clear_run(self):
        cursor = self.db_conn.execute("DELETE FROM current_run")
        cursor = self.db_conn.execute("DELETE FROM external_links")
        f = open(self.links_file_path, 'w')
        f.truncate(0)
        f.close()

        import glob
        files = glob.glob(self.links_screenshot_folder_path + '*')
        for f in files:
            os.remove(f)

        print('Run cleaned')
        if not self.continue_parsing:
            cursor = self.db_conn.execute("DELETE FROM stuff")
            print('Stuff table cleaned')

    @staticmethod
    def get_soup_from_link(link, retries=0):
        print('opening link ' + str(link))
        try:
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'}
            response = requests.get(link, headers=headers)
            response.encoding = 'utf-8'
            html = response.text
        except Exception as e:
            print(e)
            if retries > 3:
                return
            else:
                sleep(3)
                return Parser.get_soup_from_link(link, retries=retries+1)
        soup = BeautifulSoup(html, 'html.parser')
        return soup

    def parse_all_images_on_page(self, link, chan):
        bs = self.get_soup_from_link(link)
        images = bs.select('a[href] img')
        for image in images:
            img_link_prep = image.find_parent('a', href=True)
            if 'https' in quote(img_link_prep['href']):
                img_link = img_link_prep['href']
            else:
                img_link = urljoin(link, quote(img_link_prep['href']))
            parsed_img_link = urlparse(img_link)
            filename = os.path.basename(parsed_img_link.path)
            file_extension = os.path.splitext(filename)[1]

            if file_extension == '.html' or file_extension == '' or file_extension == '.webm':
                continue
            if (file_extension == '.' + str(self.parse_filetype)) or self.parse_filetype is None:
                rows = self.get_stuff_from_db(chan.id, filename)
                if len(rows) == 0:
                    try:
                        imgfile = requests.get(img_link)
                        imgfile.raise_for_status()
                    except HTTPError as e:
                        print('404 with trying to get file\n')
                        continue
                    except ConnectionError as e:
                        print('Can\'t connect to ' + str(img_link) + "\n")
                        continue
                    print('Downloading ' + str(filename))
                    open('./images/' + filename, 'wb').write(imgfile.content)
                    self.insert_stuff_into_db(filename, 'file', chan.id)
                else:
                    continue
            else:
                continue

    def insert_stuff_into_db(self, name, type, chan_id):
        fileinfo = (name, type, chan_id)
        sql = ''' INSERT INTO stuff(name,type,chan_id)
                                          VALUES(?,?,?) '''
        cur = self.db_conn.cursor()
        try:
            cur.execute(sql, fileinfo)
        except Exception as e:
            print(e)
            return

    @staticmethod
    def add_source(link):
        sql = '''INSERT INTO chans(link, enabled) VALUES(?, 1)'''
        db_conn = sqlite3.connect(Parser.db, isolation_level=None, check_same_thread=False)
        cur = db_conn.cursor()
        cur.execute(sql, (link,))
        print('source '+link + ' ADDED')

    def get_stuff_from_db(self, chan_id, stuff_name):
        cursor = self.db_conn.cursor()
        sql_select_query = """SELECT * from stuff WHERE chan_id= ? AND name= ?"""
        cursor.execute(sql_select_query, (chan_id, stuff_name))
        rows = cursor.fetchall()
        return rows

    def get_current_run_planned_links(self, chan_id):
        # db_conn = sqlite3.connect('chan', isolation_level=None)
        cursor = self.db_conn.execute(
            "SELECT * from current_run WHERE chan_id='" + str(chan_id) + "' AND completed=0")
        rows = cursor.fetchall()
        return rows

    def check_if_link_is_already_planned(self, link):
        # db_conn = sqlite3.connect('chan', isolation_level=None)
        cursor = self.db_conn.execute(
            "SELECT * from current_run WHERE link='" + str(link) + "'")
        rows = cursor.fetchall()
        return len(rows) > 0

    def plan_link_to_current_run(self, link, chan_id):
        linkinfo = (link, chan_id)
        sql = '''INSERT INTO current_run(link,chan_id) VALUES(?,?)'''
        cur = self.db_conn.cursor()
        cur.execute(sql, linkinfo)

    @staticmethod
    def ban_vidya(url):
        sql = '''INSERT INTO banned_vidya(link) VALUES(?)'''
        db_conn = sqlite3.connect(Parser.db, isolation_level=None, check_same_thread=False)
        cur = db_conn.cursor()
        cur.execute(sql, (url,))
        print(url + ' BANNED ')

    def mark_page_completed(self, link):
        self.db_conn.execute(
            "UPDATE current_run SET completed = 1 WHERE link='" + link + "' ")

    def append_to_url_parse_list(self, raw_link, chan):
        if self.is_inner_page(raw_link, chan) and raw_link is not None:
            if not bool(urlparse(raw_link).netloc):
                inner_page_link = urljoin(chan.url, quote(raw_link))
            else:
                inner_page_link = raw_link

            is_link_planned = self.check_if_link_is_already_planned(inner_page_link)
            if is_link_planned == True:
                return
            if inner_page_link != chan.url:
                self.plan_link_to_current_run(inner_page_link, chan.id)

    def is_subj_banned(self, subj):
        for ban_word in self.ban_list:
            ban_regex = r"" + re.escape(ban_word[0]) + r""
            match_ban = re.findall(ban_regex, subj, re.IGNORECASE)
            if len(match_ban) > 0:
                print(subj + '\nBanned!!!')
                return True
        return False

    def is_link_content_banned(self, link):
        # if not (link.endswith('.webm') or link.endswith('.gif')):
        #     try:
        #         html = urlopen(link).read().decode('utf8')
        #         html[:60]
        #         soup = BeautifulSoup(html, 'html.parser')
        #         title = soup.find('title')
        #         return self.is_subj_banned(title.string)
        #     except Exception as e:
        #         print(e)
        #         print('Error in checking banned content on external link')
        #         return False
        return False

    def is_vidya_banned(self, link):
        cursor = self.db_conn.execute(
            "SELECT * from banned_vidya WHERE link='" + str(link) + "'")
        rows = cursor.fetchall()
        return len(rows) > 0

    def get_ban_list(self):
        cursor = self.db_conn.execute("SELECT text from banned")
        self.ban_list = []
        for row in cursor:
            self.ban_list.append(row)

    def find_page_links(self, soup, chan):
        if soup is None:
            return
        for page_link in soup.find_all('a'):
            href = page_link.get('href')
            try:
                self.append_to_url_parse_list(href, chan)
            except Exception as e:
                print("An exception occurred")

    def parse_inner_pages(self, chan):
        if not isinstance(chan, Chan):
            raise Exception('Object is not Chan class type')

        headers = {
            'User-Agent': self.useragent,
        }
        response = requests.get(chan.url, headers=headers)
        response.raise_for_status()

        response.encoding = response.apparent_encoding
        bs = BeautifulSoup(response.text, 'html.parser')

        self.find_page_links(bs, chan)
        while True:
            current_run_pages = self.get_current_run_planned_links(chan.id)
            if len(current_run_pages) > 0:
                for current_run_page in current_run_pages:
                    self.process_page(current_run_page[1], chan)
                    GuiUtils.sleep_with_update_gui(5)
            else:
                break

    @staticmethod
    def is_thread_url(raw_link):
        thread_regex = r"(thread|res)\/\d*"
        match_thread_url = re.findall(thread_regex, raw_link)
        return len(match_thread_url) > 0

    def process_page(self, page_link, chan, retries=0):
        soup = self.check_page_response(page_link)
        if soup is None:
            return
        if self.is_thread_url(page_link):
            subj = self.get_thread_subj(soup)
            if self.is_subj_banned(subj):
                print('Banned subj - "' + subj + '", skipping')
                self.mark_page_completed(page_link)
                return
            if self.mode == 'parse':
                print('searching for files on page ' + page_link + '\n')
                self.parse_all_images_on_page(page_link, chan)
                # Thread(target=self.parse_all_images_on_page, args=(page_link, chan)).start()
            elif self.mode in ['xspf', 'm3u', 'm3u_SFW' ]:
                print('searching for links on page ' + page_link + '\n')
                self.build_playlist_links(chan, soup, page_link)
                GuiUtils.sleep_with_update_gui(5)

        self.find_page_links(soup, chan)
        self.mark_page_completed(page_link)

    def get_thread_subj(self, soup):
        if soup is None:
            subj = ''
        subj_soup = soup.select_one('.post.op span.subject')
        if subj_soup is None:
            subj_soup = soup.select_one('.post.op blockquote.postMessage')
        if subj_soup is not None:
            subj = subj_soup.text
        else:
            subj = ''
        print(subj)
        return subj

    def check_page_response(self, page_link):
        soup = self.get_soup_from_link(page_link)

        if soup is None:
            print('ALARM !! Soup is NONE on page ' + str(page_link))

            # @todo make this feature - retrying page response

            self.mark_page_completed(page_link)
            GuiUtils.sleep_with_update_gui(5)
            return

        return soup

    def is_link_external(self, link, chan):
        is_inner = self.is_inner_page(link, chan)
        exclude_regex = r"4channel\.org|google\.com|4chan\.org|iqdb\.org|imgops\.com|lainchan|8kun|youtube|youtu\.be|\.js|vocaroo"
        match_excluded = re.findall(exclude_regex, link)
        return not is_inner and len(match_excluded) == 0

    def build_playlist_links(self, chan, bs, page_link):
        if bs is None:
            print('ERR!!! SOUP IS NONE')
            return
        tags_with_hyperlinks = bs.find_all('a', href=True)
        for tag in tags_with_hyperlinks:
            try:
                if self.is_vidya_link(tag['href']):
                    self.process_vidya_link(tag['href'], chan, page_link)
            except Exception as e:
                continue

        spans_that_can_contain_links = bs.find_all(text=re.compile("youtube.com|youtu.be"))
        for span in spans_that_can_contain_links:
            raw_link = span.parent.text
            raw_link = re.sub(r'.*https', 'https', raw_link)
            raw_link = re.sub(r'((v=|be\/).{11}).*', r'\1', raw_link)
            if self.is_vidya_link(raw_link):
                self.process_vidya_link(raw_link, chan, page_link)

    def process_vidya_link(self, raw_link, chan, page_link):
        vidya_link = self.get_valid_vidya_link(raw_link, chan.url)
        if vidya_link is not None:
            if not self.is_link_content_banned(vidya_link) and not self.is_vidya_banned(vidya_link):
                rows = self.get_stuff_from_db(chan.id, vidya_link)
                if len(rows) == 0:
                    print(raw_link)
                    print('pew! it is VIDYA !!!')
                    if self.mode == 'xspf':
                        self.XspfPlaylist.add_track(vidya_link, page_link)
                    else:
                        self.M3UPlaylist.add_track(vidya_link, page_link)
                    GuiUtils.update_gui()
                    self.insert_stuff_into_db(vidya_link, 'vidya', chan.id)
                    GuiUtils.update_gui()

    @staticmethod
    def get_valid_vidya_link(raw_link, chan_url):
        vidya_link = raw_link
        link_url_components = urlparse(vidya_link)
        is_inner_link = not bool(link_url_components.scheme) and not bool(link_url_components.netloc)
        if is_inner_link and (vidya_link.endswith('.webm') or vidya_link.endswith('.gif')):
            vidya_link = urljoin(chan_url, quote(vidya_link))
        elif not is_inner_link and (vidya_link.endswith('.webm') or vidya_link.endswith('.gif')):
            if chan_url == 'https://lainchan.org/':
                return None
            else:
                if not bool(link_url_components.scheme):
                    valid_link = 'https:' + vidya_link
                else:
                    valid_link = raw_link
                return valid_link
        return vidya_link

    def is_vidya_link(self, link):
        if link is None:
            return False
        regex = r"youtube\.com|youtu\.be|vimeo\.com|\.webm|\.gif|\.mp4"
        if self.mode == 'm3u_SFW':
            regex = r"youtube\.com|youtu\.be"
        match_vidya_link = re.findall(regex, link)
        exclude_regex = r"channel|playlist|player\.php|iqdb\.org|imgops\.com|\&list"
        match_excluded = re.findall(exclude_regex, link)
        return len(match_vidya_link) > 0 and len(match_excluded) == 0

    @staticmethod
    def is_inner_page(link, chan):
        if not isinstance(chan, Chan):
            raise Exception('Object is not Chan class type')

        if link == chan.url:
            return False

        excluded_links = ['/search', './archive']
        if link is None or link in excluded_links:
            return False

        excluded_link_parts = ['page_mode/', 'search/image/', '/gallery/']
        for link_part in excluded_link_parts:
            if link_part in link:
                return False

        regex = r"html\#\d*|javascript|\#"
        match_thread_answer = re.findall(regex, link)
        not_thread_answer = len(match_thread_answer) == 0
        if not not_thread_answer:
            return False

        link_url_components = urlparse(link)
        chan_url_components = urlparse(chan.url)
        if chan.scope == 'topic':
            regex_topic = r"" + re.escape(chan.topic) + r""
            match_topic = re.findall(regex_topic, link)

            is_4chan_r = r"4chan"
            match_4chan = re.findall(is_4chan_r, chan.url)

            is_lainchan_r = r"lainchan"
            match_lainchan = re.findall(is_lainchan_r, chan.url)

            if not link_url_components.netloc and len(match_4chan) > 0:
                if Parser.is_thread_url(link):
                    return True
            if len(match_topic) == 0:
                return False
            is_pointing_to_same_location = not bool(link_url_components.netloc) \
                                           or (bool(link_url_components.netloc) is True and (
                    link_url_components.netloc == chan_url_components.netloc))
            by_location = bool(is_pointing_to_same_location)
        else:
            by_location = not bool(urlparse(link).netloc)

        # @todo do smth with that
        regex = r"\.webm|\.png|\.jpg|\.gif|\.jpeg|\.mp4"
        match_file = re.findall(regex, link)
        is_inner_page = bool(by_location) and len(match_file) == 0

        if not link_url_components.netloc and len(match_lainchan) > 0:
            lainchan_inner_page_match = link.endswith('.html')
            return is_inner_page and lainchan_inner_page_match
        else:
            return is_inner_page

    def run(self):
        for board in self.chans:
            chan = Chan(board[0], board[1])

            th = Thread(target=self.parse_inner_pages, args=(chan,),
                        name="thread_chan_parsing_with_chan_id_" + str(chan.id))
            th.start()
            GuiUtils.sleep_with_update_gui(5)
