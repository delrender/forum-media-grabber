from classes.Parser import Parser
import re
from time import sleep
import os


class ParserLinks(Parser):
    filepath = os.path.dirname(os.path.abspath(__file__))
    links_file_path = os.path.join(filepath, '../links.html')
    links_screenshot_folder_path = os.path.join(filepath, '../links_screenshots/')
    link_counter = 1

    def __init__(self, mode, parse_filetype=None, continue_parsing=None, threads=True):
        super().__init__(mode, parse_filetype, continue_parsing, threads)

    def mark_link_parsed(self, link):
        sql = "INSERT INTO external_links(link) VALUES(?)"
        cur = self.db_conn.cursor()
        cur.execute(sql, (str(link),))

    def is_link_already_parsed(self, link):
        cursor = self.db_conn.cursor()
        sql_select_query = "SELECT * from external_links WHERE link =?"
        cursor.execute(sql_select_query, (str(link),))
        rows = cursor.fetchall()
        return len(rows) > 0

    def get_all_external_links(self, chan, bs):
        if bs is None:
            print('ERR!!! SOUP IS NONE')
            return

        urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+',
                          str(bs.html))
        for url in urls:
            if self.is_link_external(url, chan):
                clean_url = re.sub(r'<wbr\/>', '', url)
                clean_url = clean_url.split("<br/>")[0]
                clean_url = clean_url.split("</")[0]

                if not self.is_link_already_parsed(clean_url):
                    print(clean_url)
                    self.mark_link_parsed(clean_url)
                    try:
                        self.driver.get(clean_url)
                        sleep(1)
                        self.driver.get_screenshot_as_file(self.links_screenshot_folder_path + str(self.link_counter) + ".png")
                        f = open(self.links_file_path, "a+")
                        f.write("<a href='" + clean_url + "' target='_blank'>" + "<img src='./links_screenshots/" + str(
                            self.link_counter) + ".png' width=500 height=300>" + clean_url + "</a><br/>\n")
                        f.close()
                        self.link_counter += 1
                    except Exception as e:
                        print(e)

    def process_page(self, page_link, chan, retries=0):
        soup = self.check_page_response(page_link)
        if soup is None:
            return
        if self.is_thread_url(page_link):
            subj = self.get_thread_subj(soup)
            if self.is_subj_banned(subj):
                print('Banned subj - "' + subj + '", skipping')
                self.mark_page_completed(page_link)
                return

            self.get_all_external_links(chan, soup)

        self.find_page_links(soup, chan)
        self.mark_page_completed(page_link)
        sleep(1)
