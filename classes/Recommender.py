from classes.XspfPlaylist import XspfPlaylist
import imageio
import visvis as vv
from PIL import Image
import requests
from io import BytesIO

class Recommender:
    def __init__(self):
        self.XspfPlaylist = None

    def get_images_from_playlist(self):
        print('Getting images from xspf playlist')
        self.XspfPlaylist = XspfPlaylist('./lain.xspf', 'continue')
        playlist_items = self.XspfPlaylist.tracklist.getchildren()
        for item in playlist_items:
            location = item.find('{http://xspf.org/ns/0/}location')
            print(location.text)
            response = requests.get(location.text)
            im = Image.open(BytesIO(response.content))
            for i, frame in enumerate(self.iter_frames(im)):
                frame.save('test%d.png' % i, **frame.info)
            pass

    def iter_frames(self, im):
        try:
            i = 0
            while 1:
                im.seek(i)
                imframe = im.copy()
                if i == 0:
                    palette = imframe.getpalette()
                else:
                    imframe.putpalette(palette)
                yield imframe
                i += 1
        except EOFError:
            pass


