from lxml import etree as ET
import threading

lock = threading.Lock()


class XspfPlaylist:

    template = ''

    def __init__(self, filepath, continue_parsing=None):
        self.filepath = filepath
        self.continue_parsing = continue_parsing
        self.xml_doc = None
        self.root_xml_doc = None
        self.tracklist = None
        self.init_xml_playlist(self.filepath)

    def get_template(self):
        self.template = '''<playlist xmlns="http://xspf.org/ns/0/" xmlns:vlc="http://www.videolan.org/vlc/playlist/ns/0/" version="1">
                                <title>&#1055;&#1083;&#1077;&#1081;&#1083;&#1080;&#1089;&#1090;</title>
                                <trackList>
                                </trackList>
                                <extension application="http://www.videolan.org/vlc/playlist/0">
                                    <vlc:item tid="0"/>
                                </extension>
</playlist>'''

    def init_xml_playlist(self, filepath):
        if not self.continue_parsing:
            self.get_template()
            with open(filepath, "w") as text_file:
                text_file.write(self.template)
        self.xml_doc = ET.parse(filepath)
        self.root_xml_doc = self.xml_doc.getroot()
        self.tracklist = self.root_xml_doc.find('{http://xspf.org/ns/0/}trackList')
        print('Playlist initiated')

    def add_track(self, link=None, page_link=None):
        track = ET.Element('track')
        location = ET.Element('location')
        location.text = str(link)
        track.append(location)

        annotation = ET.Element('annotation')
        annotation.text = str(page_link)
        track.append(annotation)

        title = ET.Element('title')
        title.text = str(page_link)
        track.append(title)

        extension = ET.Element('extension')
        extension.set('application', 'http://www.videolan.org/vlc/playlist/0')
        ext_id = ET.SubElement(extension, '{vlc}id')
        tracklist_length = len(self.tracklist.getchildren())
        ext_id.text = str(tracklist_length + 1)
        track.append(extension)

        self.tracklist.append(track)
        print('track added!!!\n')
        # ET.dump(self.tracklist)
        self.xml_doc.write(self.filepath)
