#!/usr/bin/env python

from functools import partial
import os
import sys
import sqlite3
from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5.QtWidgets import QInputDialog

import design
from classes.Parser import Parser
from classes.ParserLinks import ParserLinks
import mpv
from threading import *
from classes.M3UPlaylist import M3UPlaylist
import requests
from requests.exceptions import HTTPError, ConnectionError
from urllib.parse import urljoin, urlparse, quote
from PyQt5 import QtGui


class ChanStuffMinerApp(QtWidgets.QMainWindow, design.Ui_MainWindow):
    filepath = os.path.dirname(os.path.abspath(__file__))
    db = os.path.join(filepath, 'chan')
    chans = []

    def __init__(self):
        super().__init__()
        import locale
        locale.setlocale(locale.LC_NUMERIC, 'C')
        self.db_conn = sqlite3.connect(self.db, isolation_level=None, check_same_thread=False)
        self.setupUi(self)
        self.place_chans()
        self.commandLinkButton.clicked.connect(self.run_parser)
        self.commandLinkButton_2.clicked.connect(self.exit)
        self.commandLinkButton_3.clicked.connect(self.watch)
        self.pushButton.clicked.connect(partial(self.toggle_all_chans, True))
        self.pushButton_2.clicked.connect(partial(self.toggle_all_chans, False))
        self.addSourceButton.clicked.connect(self.showAddSourceDialog)

    def showAddSourceDialog(self):
        source_url, ok = QInputDialog.getText(self, 'Input Dialog',
                                        'Add Source Url:')

        if ok:
            Parser.add_source(source_url)

    def toggle_all_chans(self, checked):
        widgets = (self.ChansList.itemAt(i).widget() for i in range(self.ChansList.count()))
        for widget in widgets:
            if isinstance(widget, QtWidgets.QCheckBox):
                if checked:
                    widget.setChecked(True)
                else:
                    widget.setChecked(False)

    def watch(self):
        print('Yeah! Watching!')
        self.player = mpv.MPV(ytdl=True, input_default_bindings=True, input_vo_keyboard=True, osc=True)
        self.player.loop = True
        self.player.geometry = '200x200'
        self.player.loadlist(os.path.join(self.filepath, 'lain.m3u'))
        self.player.playlist_shuffle()
        th = Thread(target=self.player.wait_for_playback)
        th.start()

        player = self.player

        @player.on_key_press('Alt+3')
        def ban():
            link = get_current_file_url()
            Parser.ban_vidya(link)
            M3UPlaylist.remove_track(link)

        def get_current_file_url():
            return player._get_property('path')

        @player.on_key_press('Alt+2')
        def copy_url():
            link = get_current_file_url()
            QtGui.QGuiApplication.clipboard().setText(link)
            print('URL COPIED TO CLIPBOARD!')

        @self.player.on_key_press('Alt+1')
        def next_vidya():
            ban()
            player.playlist_next()

        @player.on_key_press('Alt+5')
        def save():
            link = get_current_file_url()
            parsed_img_link = urlparse(link)
            filename = os.path.basename(parsed_img_link.path)
            try:
                remote_file = requests.get(link)
                remote_file.raise_for_status()
            except HTTPError as e:
                print('404 with trying to get file\n')
            except ConnectionError as e:
                print('Can\'t connect to ' + str(link) + "\n")
            print('Downloading ' + str(link))
            open(self.filepath+'/downloads/' + filename, 'wb').write(remote_file.content)

    def set_chan_state(self, chan_row, chbx):
        print(chan_row[1])
        print(chan_row[2])
        state = 0
        if chbx.isChecked():
            state = 1
        self.db_conn.execute(
            "UPDATE chans SET enabled = " + str(state) + " WHERE link='" + chan_row[1] + "' ")

    def place_chans(self):
        self.get_all_chans()
        _translate = QtCore.QCoreApplication.translate
        row_counter = 1
        col_counter = 0
        for chan in self.chans:
            # chbx = QtWidgets.QCheckBox()
            chbx = QtWidgets.QCheckBox(self.centralwidget)
            chbx.setText(_translate("MainWindow", chan[1]))
            if chan[2] == 1:
                chbx.setChecked(True)
            chbx.stateChanged.connect(partial(self.set_chan_state, chan, chbx))
            self.ChansList.addWidget(chbx, row_counter, col_counter)

            col_counter += 1
            if col_counter >= 4:
                col_counter = 0
                row_counter += 1

    def create_app_tables(self):
        sql = ''' BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "banned_vidya" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"link"	TEXT
);
CREATE TABLE IF NOT EXISTS "external_links" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"link"	VARCHAR UNIQUE
);
CREATE TABLE IF NOT EXISTS "banned_boards" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"board"	TEXT NOT NULL UNIQUE
);
CREATE TABLE IF NOT EXISTS "stuff" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"name"	TEXT UNIQUE,
	"type"	VARCHAR,
	"subtype"	VARCHAR,
	"chan_id"	INTEGER
);
CREATE TABLE IF NOT EXISTS "current_run" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"link"	VARCHAR UNIQUE,
	"in_progress"	BOOLEAN DEFAULT (0),
	"completed"	BOOLEAN DEFAULT (0),
	"chan_id"	INT
);
CREATE TABLE IF NOT EXISTS "banned" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"text"	VARCHAR UNIQUE
);
CREATE TABLE IF NOT EXISTS "chans" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"link"	VARCHAR UNIQUE,
	"enabled"	BOOLEAN DEFAULT (0)
);
CREATE TABLE IF NOT EXISTS "parse_modes" (
	"id"	INTEGER,
	"name"	VARCHAR UNIQUE,
	PRIMARY KEY("id")
);
COMMIT;
'''
        cursor = self.db_conn.executescript(sql)

    def get_all_chans(self):
        try:
            cursor = self.db_conn.execute("SELECT * from chans")
        except sqlite3.OperationalError:
            self.create_app_tables()
            return
        self.chans = []
        for row in cursor:
            self.chans.append(row)

    def exit(self):
        sys.exit()

    def run_parser(self):
        self.get_mode()
        if self.mode == 'links':
            print('Fetching all external links! - under development')
            self.parser = ParserLinks(self.mode, continue_parsing=self.checkBox.isChecked())
        else:
            self.parser = Parser(self.mode, continue_parsing=self.checkBox.isChecked())

        if self.mode == 'parse':
            print('Heey ho! Parsing!')
        if self.mode == 'xspf':
            print('Heey ho! Fetching VIDYA links!')
        self.parser.run()

    def get_mode(self):
        group_elements = self.groupBox.children()
        radio_buttons = [elem for elem in group_elements if isinstance(elem, QtWidgets.QRadioButton)]
        for rb in radio_buttons:
            if rb.isChecked():
                pre_mode = rb.text()
        if pre_mode == 'files':
            self.mode = 'parse'
        else:
            self.mode = pre_mode
        return self.mode


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = ChanStuffMinerApp()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
